# Source code for Account Sales Prediction
# Author: Priyang Tripathi- Data Science Associate

import torch
import torch.nn as nn

from sklearn.metrics import r2_score, mean_absolute_error
import numpy as np
import datetime
import matplotlib.pyplot as plt
import pandas as pd

import time

start = time.time()

pd.options.display.width = None
np.set_printoptions(suppress=True, linewidth=np.nan, threshold=None)


# Below is the pytorch class that defines the the multilayer perceptron (MLP) architecture
# (number of hidden layers, neurons, activation function, etc)

class mlp(torch.nn.Module):
  def __init__(self, input, hidden):
    super(mlp, self).__init__()  # initialize
    self.classifier = nn.Sequential(  # Sequential is used when combining different layers
      nn.Linear(input, hidden),
      # Input feature matrix is converted into a matrix with shape (hidden) and linearly transformated
      nn.ReLU(),  # Activation function is applied
      nn.Linear(hidden, hidden),  # Result of previous layer is linearly transformaed
      nn.ReLU(),  # Activation function is applied
      nn.Linear(hidden, hidden),  # Result of previous layer is linearly transformaed
      nn.ReLU(),  # Activation function is applied
      nn.Linear(hidden, hidden),  # Result of previous layer is linearly transformaed
      nn.ReLU(),  # Activation function is applied
      nn.Linear(hidden, 8))  # At the final layer, one output is given (Trip amount)

  def forward(self, x):
    x = self.classifier(x)  # the input is sent throught the MLP architecture defined above
    monthly_sales = torch.sum(x, dim=1).reshape(-1, 1)
    a = torch.zeros(0, 1)
    for i in torch.split(x, 928):
      a = torch.cat((a, i.sum(0).view(8, 1)))
    a = torch.cat((a, monthly_sales))
    return [torch.abs(a), torch.abs(x)]


def train(optimizer, model, criterion, inputs, labels):
  model.train()  # Model is in the training mode (meaning gradients are calculated)
  optimizer.zero_grad()  # Gradients are zeroed
  pred = model(inputs)
  nat_pred = pred[0]
  # acc_pred = pred[1]
  loss = criterion(nat_pred, labels)  # Calculate loss between prediction and label
  loss.backward()  # Backpropagate the gradients
  optimizer.step()  # (I dont fully know what happens with this code)

  # if epoch % 500 == 0:
  #   df = pd.DataFrame(acc_pred,
  #                     columns=['pred_KLC', 'pred_KML', 'pred_KUC', 'pred_KRC', 'pred_KHN', 'pred_KES', 'pred_KMS',
  #                              'pred_KHL'])
  #   df = pd.concat([acc_info_train, df], axis=1)
  #   df.to_csv("model_4_training_results_epoch_{}.csv".format(epoch), encoding='utf-8', index=False)
  #
  return loss.item()  # Return loss


def test(model, inputs, labels, epoch):
  model.eval()  # Mode is in evaluation mode: no gradients are calcualted
  with torch.no_grad():  # In tensorflow if tensor has a parameter "autograd:true" then, gradients are calculated. This code sets the autograd to false for all tensors below
    pred = model(inputs)  # Get prediction
    nat_pred = pred[0]
    acc_pred = pred[1].detach().numpy()
    r2 = r2_score(labels, nat_pred)  # Calculate R2
    p = nat_pred.detach().numpy()
    l = labels.detach().numpy()
    # print(p[:5, :].astype(int))
    # print(l[:5, :].astype(int))
    # print("-------")

    # if epoch % 100 == 0:
    #   df = pd.DataFrame(acc_pred,
    #                     columns=['pred_KLC', 'pred_KML', 'pred_KUC', 'pred_KRC', 'pred_KHN', 'pred_KES', 'pred_KMS',
    #                              'pred_KHL'])
    #   df = pd.concat([acc_info_test, df], axis=1)
    #   df.to_csv("model_4_results_epoch_{}.csv".format(epoch), encoding='utf-8', index=False)

    mape = np.mean(np.divide(np.abs(l - p), l, out=np.zeros_like(np.abs(l - p)), where=l != 0)) * 100
    mae = mean_absolute_error(labels, nat_pred)
    plt.scatter(labels, nat_pred)
    axes = plt.gca()
    axes.set_xlabel("Actual sales")
    axes.set_ylabel("Estimated sales")
    # axes.set_xlim([min(labels), max(labels)])
    # axes.set_ylim([min(labels), max(labels)])
    print(epoch, "-","r2 is ",r2)
    plt.title(r'Epoch: {}  |  $R^2$: {}'.format(epoch, round(r2, 2)))
    plt.draw()
    plt.pause(0.00000000000000001)
    plt.cla()
    return [r2, mape, mae]


db = pd.read_csv("db_7.csv", dtype=str, encoding='utf-8')


db = db[~(db['Y'] == "2017")]
db = db[~((db['Y'] == "2018") & (db['M'] == "1"))]
db = db[~((db['Y'] == "2018") & (db['M'] == "2"))]
db = db[~((db['Y'] == "2018") & (db['M'] == "3"))]

cols = db.columns.tolist()
acc_info = db[cols]

print(cols)

indications = ["KLC", "KML", "KUC", "KRC", "KHN", "KES", "KMS", "KHL"]

for i in cols:
  if i not in ["Ins_Code", "Ins_DCF_Code", "HP_GP_TYPE", "Y", "M"] and i not in indications:
    db[i] = pd.to_numeric(db[i])
    if i == "KEY_Inhouse_QTY":
      qty_max = db[i].max()
    db[i] = db[i] / db[i].max()
    db[i] = db[i].fillna(0)

db.drop(["Ins_Code", "Ins_DCF_Code", "HP_GP_TYPE", "KEY_Inhouse_JPY"], axis=1, inplace=True)

# cols = ["Y", "M", 'KLC', 'KML', 'KUC', 'KRC', 'KHN', 'KES', 'KMS', 'KHL',
#         'KEY_Inhouse_QTY', 'OPDIVO', 'ERBITUX', 'YERVOY', 'BAVENCIO',
#         'ALIMTA', 'AVASTIN', 'TECENTRIQ', 'TKIs', 'MEKINIST+TAFINLAR',
#         'KLC_time', 'KML_time', 'KHL_time', 'KUC_time', 'KMS_time', 'KRC_time',
#         'KHN_time', 'KES_time', 'cancer_reg_KLC', 'cancer_reg_KES',
#         'cancer_reg_KUC', 'cancer_reg_KHN', 'cancer_reg_KRC', 'dpc_KLC',
#         'dpc_KML', 'dpc_KUC', 'dpc_KHL', 'dpc_KRC', 'dpc_KHN', 'dpc_KMS',
#         'dpc_KES']
cols = ["Y", "M", 'KLC', 'KML', 'KUC', 'KRC', 'KHN', 'KES', 'KMS', 'KHL',
        'KEY_Inhouse_QTY', 'OPDIVO', 'ERBITUX', 'YERVOY', 'BAVENCIO',
        'ALIMTA', 'AVASTIN', 'TECENTRIQ', 'TKIs', 'MEKINIST+TAFINLAR',
        'KLC_time', 'KML_time', 'KHL_time', 'KUC_time', 'KMS_time', 'KRC_time',
        'KHN_time', 'KES_time', 'cancer_reg_KLC', 'cancer_reg_KES',
        'cancer_reg_KUC', 'cancer_reg_KHN', 'cancer_reg_KRC', 'dpc_KLC',
        'dpc_KML', 'dpc_KUC', 'dpc_KHL', 'dpc_KRC', 'dpc_KHN', 'dpc_KMS',
        'dpc_KES','clusters']


db = db[cols].apply(pd.to_numeric, errors='coerce')
db = db.sort_values(["Y", "M"])
acc_info["Y"] = pd.to_numeric(acc_info["Y"])
acc_info["M"] = pd.to_numeric(acc_info["M"])
acc_info = acc_info.sort_values(["Y", "M"])

labels = db[["Y", "M", 'KLC', 'KML', 'KUC', 'KRC', 'KHN', 'KES', 'KMS', 'KHL']].drop_duplicates(["Y", "M"])
db.drop(["Y", "M", 'KLC', 'KML', 'KUC', 'KRC', 'KHN', 'KES', 'KMS', 'KHL'], axis=1, inplace=True)
labels.drop(["Y", "M"], axis=1, inplace=True)

acc_lab = db[["KEY_Inhouse_QTY"]] * qty_max

db = torch.FloatTensor(db.values)
labels = torch.FloatTensor(labels.values)
acc_labels = torch.FloatTensor(acc_lab.values)

# 1 month : 928 accounts

split = int((len(db) / 928 - 6) * 928)
train_labels = labels[:len(labels) - 6, :].reshape(28 * 8, 1) #reshape worked rather than view
test_labels = labels[len(labels) - 6:, :].reshape(6 * 8, 1)

train_labels = torch.cat((train_labels, acc_labels[:split, :]))
test_labels = torch.cat((test_labels, acc_labels[split:, :]))


train_inputs = db[:split, :]
test_inputs = db[split:, :]

# acc_info_train = acc_info.head(breaker).reset_index()
acc_info_test = acc_info.tail(len(acc_info) - int((len(db) / 928 - 6) * 928)).reset_index()

model = mlp(len(cols) - 10, 128)
optimizer = torch.optim.Adam(model.parameters(), lr=0.01)
criterion = torch.nn.MSELoss()


# for epoch in range(1, 2001):
  # loss = train(optimizer, model, criterion, train_inputs, train_labels)
  # [r2, mape, mae] = test(model, test_inputs, test_labels, epoch)

  # print(epoch, loss, r2, mape, mae)

end = time.time()

print((end - start)/60, "minutes")